#ifndef __GWR_LOGGER_H__
#define __GWR_LOGGER_H__
//  ............................................................................
#include    <stdio.h>
#include    <wx/log.h>
//  ............................................................................
#define     GWR_LOG_WX(FORMAT, ...)                                             \
{                                                                               \
    wxLog::SetActiveTarget(s_logger);                                           \
    wxLogMessage(_T(FORMAT), __VA_ARGS__);                                      \
}

#define     GWR_LOG_PRINTF(FORMAT, ...)                                         \
    printf("%s\n", wxString::Format( wxString::FromUTF8(FORMAT), __VA_ARGS__ ).mb_str().data());
//  ............................................................................
#ifdef  GWR_DEBUG_WXLOG
    #define GWR_TRC(FORMAT, ...)    GWR_LOG_WX("trc:" FORMAT, __VA_ARGS__);
    #define GWR_LOG(FORMAT, ...)    GWR_LOG_WX("log:" FORMAT, __VA_ARGS__);
    #define GWR_INF(FORMAT, ...)    GWR_LOG_WX("inf:" FORMAT, __VA_ARGS__);
    #define GWR_WNG(FORMAT, ...)    GWR_LOG_WX("Wng:" FORMAT, __VA_ARGS__);
    #define GWR_ERR(FORMAT, ...)    GWR_LOG_WX("ERR:" FORMAT, __VA_ARGS__);
#endif

#ifdef  GWR_DEBUG_PRINTF
    #define GWR_TRC(FORMAT, ...)    GWR_LOG_PRINTF("trc:" FORMAT, __VA_ARGS__);
    #define GWR_LOG(FORMAT, ...)    GWR_LOG_PRINTF("log:" FORMAT, __VA_ARGS__);
    #define GWR_INF(FORMAT, ...)    GWR_LOG_PRINTF("inf:" FORMAT, __VA_ARGS__);
    #define GWR_WNG(FORMAT, ...)    GWR_LOG_PRINTF("Wng:" FORMAT, __VA_ARGS__);
    #define GWR_ERR(FORMAT, ...)    GWR_LOG_PRINTF("ERR:" FORMAT, __VA_ARGS__);
#endif
//  ............................................................................
class   GwrSpaces
{
    private:
    int         a_steps_card;
    wxString    a_spaces;
    wxString    a_step;
    size_t      a_step_length;

    public:
    const   wxString    &       get()   { return a_spaces;      }
            int                 stc()   { return a_steps_card;  }

    void    inc()
        {
            a_spaces.Append(a_step);
            a_steps_card++;
        }
    void    dec()
        {
            if ( a_spaces.Len() >= a_step_length )
            {
                a_spaces.Replace(
                        a_step                  ,
                        wxString::FromUTF8("")  ,
                        false                   );
                a_steps_card--;
            }
            else
            {
                GWR_LOG_PRINTF("ERR:GwrSpaces::dec():[%i]", a_spaces.Len());
            }
        }

    public:
    GwrSpaces()
    {
        a_steps_card    =   0;
        a_step = wxString::FromUTF8("    ");
        a_step_length   =   a_step.Len();
    }
};


#endif                                                                          //  __GWR_LOGGER_H__
