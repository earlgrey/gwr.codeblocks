MFilesCard=008
NFilesCard=000

MFiles[0]="Makefile.am"                                               MRPaths[0]="src/sdk/wxscintilla"

MFiles[1]="Scintilla.iface"                                           MRPaths[1]="src/sdk/wxscintilla/src/scintilla/include"

MFiles[2]="SciLexer.h"                                                MRPaths[2]="src/sdk/wxscintilla/src/scintilla/include"

MFiles[3]="Catalogue.cxx"                                             MRPaths[3]="src/sdk/wxscintilla/src/scintilla/src"

MFiles[4]="wxscintilla.h"                                             MRPaths[4]="src/sdk/wxscintilla/include/wx"

MFiles[5]="editorcolourset.h"                                         MRPaths[5]="src/include"

MFiles[6]="editorcolourset.cpp"                                       MRPaths[6]="src/sdk"

MFiles[7]="editorlexerloader.cpp"                                     MRPaths[7]="src/sdk"
