#!/bin/bash

#   ############################################################################
#   src/build.bash
#
#   Build a particular version of scite with Scintilla-LexerExt
#   ############################################################################

#   ############################################################################
#   INCLUDES
#   ############################################################################
source  /usr/local/bin/gwr.cb-build-helper.include.bash
#   ############################################################################
#   VARIABLES
#   ############################################################################
NmDir="${SlcRootDir}/src/${SlcCbVersion}"
ScDir="${NmDir}/trunk"
#   ############################################################################
#   FUNCTIONS
#   ############################################################################
#   ############################################################################
#   MAIN
#   ############################################################################
#   ============================================================================
#   describe & import : new / mod files from this particular version of codeblocks
#   ============================================================================
cbh_func__log "> describe & import new/mod files from codeblocks ( '${SlcCbVersion}' )"

SleFileD="${NmDir}/nm-files/mod"

#   scintilla-LexerExt : "std" files
SleFileN="Makefile.am"
SleFileR="src/sdk/wxscintilla"
cbh_func__df_add_mod    "${NmDir}" "${SleFileN}" "${SleFileR}"
cbh_func__check_errors  $?

SleFileN="Scintilla.iface"
SleFileR="src/sdk/wxscintilla/src/scintilla/include"
cbh_func__df_add_mod    "${NmDir}" "${SleFileN}" "${SleFileR}"
cbh_func__check_errors  $?

SleFileN="SciLexer.h"
SleFileR="src/sdk/wxscintilla/src/scintilla/include"
cbh_func__df_add_mod    "${NmDir}" "${SleFileN}" "${SleFileR}"
cbh_func__check_errors  $?

SleFileN="Catalogue.cxx"
SleFileR="src/sdk/wxscintilla/src/scintilla/src"
cbh_func__df_add_mod    "${NmDir}" "${SleFileN}" "${SleFileR}"
cbh_func__check_errors  $?

SleFileN="wxscintilla.h"
SleFileR="src/sdk/wxscintilla/include/wx"
cbh_func__df_add_mod    "${NmDir}" "${SleFileN}" "${SleFileR}"
cbh_func__check_errors  $?

#   scintilla-LexerExt : c::b modifications
SleFileN="editorcolourset.h"
SleFileR="src/include"
cbh_func__df_add_mod    "${NmDir}" "${SleFileN}" "${SleFileR}"
cbh_func__check_errors  $?

SleFileN="editorcolourset.cpp"
SleFileR="src/sdk"
cbh_func__df_add_mod    "${NmDir}" "${SleFileN}" "${SleFileR}"
cbh_func__check_errors  $?

SleFileN="editorlexerloader.cpp"
SleFileR="src/sdk"
cbh_func__df_add_mod    "${NmDir}" "${SleFileN}" "${SleFileR}"
cbh_func__check_errors  $?
#   ============================================================================
#   export new / mod files from their description
#   ============================================================================
cbh_func__df_export     "${NmDir}" "${ScDir}"
cbh_func__check_errors  $?
#   ============================================================================
#   build !
#   ============================================================================
cbh_func__log "> building codeblocks ( '${SlcCbVersion}' )"

cbh_func__cd            "" "${ScDir}"
cbh_func__check_errors  $?

make -j 2

exit 0

