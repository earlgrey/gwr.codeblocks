#!/bin/bash

#   ############################################################################
#   src/build.bash
#
#   ############################################################################

#   ############################################################################
#   INCLUDES
#   ############################################################################
source  /usr/local/bin/gwr.cb-build-helper.include.bash
#   ############################################################################
#   VARIABLES
#   ############################################################################
NmDir="${SlcRootDir}/src/${SlcCbVersion}"
#   ############################################################################
#   FUNCTIONS
#   ############################################################################

#   ############################################################################
#   MAIN
#   ############################################################################
#   ============================================================================
#   eventually extract codeblocks archive
#   _GWR_TODO_ mkdir files-new / modified / original, lexerext,
#       symlinks to scite code, copy original files
#   ============================================================================
if [[ ! -d "${SlcCbVersion}" ]] ; then

    SlcCbArchive="${SlcCbVersion}.tgz"
    cbh_func__log "> directory ${SlcRootDir}/src/${SlcCbVersion} not present, extracting from [${SlcCbArchive}] ..."
    exit 1

    if [[ !  -f "${SlcCbArchive}" ]] ; then
        cbh_func__log "> archive [${SlcCbArchive}] not present, exiting"
        exit 1
    fi

else

    cbh_func__log "> directory ${SlcRootDir}/src/${SlcCbVersion} present"

fi
#   ============================================================================
#   reset description of new/mod files
#   ============================================================================
cbh_func__df_reset  "${NmDir}"
#   ============================================================================
#   include Scintilla-LexerExt ? ( splitted in a separate file for lisibility )
#   ============================================================================
if [[ "${SlcBuildLex}" = "yes" ]] ; then
    cbh_func__cd_and_build  ""  "./" "build-gwr.scintilla-lexer-ext.bash"
    cbh_func__check_errors  $?
fi
#   ============================================================================
#   include OpenFilesList modification ?
#   ============================================================================
if [[ "${SlcBuildOfl}" = "yes" ]] ; then
    cbh_func__df_import     "${NmDir}" "${SlcRootDir}/src/gwr.OpenFilesList"
    cbh_func__check_errors  $?
fi
#   ============================================================================
#   describe & import new/mod files from codeblocks::gwr.Logger
#   ============================================================================
cbh_func__log "> describe & import new/mod files from codeblocks::gwr.Logger"
cbh_func__df_import     "${NmDir}" "${SlcRootDir}/src/gwr.Logger"
cbh_func__check_errors  $?
#   ============================================================================
#   describe & import new/mod files from codeblocks::gwr.bug-001
#   ============================================================================
cbh_func__log "> describe & import new/mod files from codeblocks::gwr.bug-001"
cbh_func__df_import     "${NmDir}" "${SlcRootDir}/src/gwr.bug-001"
cbh_func__check_errors  $?
#   ============================================================================
#   Launch script for building codeblocks
#   ============================================================================
cbh_func__cd_and_build "" "${SlcRootDir}/src/${SlcCbVersion}"
cbh_func__check_errors $?
#   ============================================================================
#   update non-language-dependant links that system-links point on ( if no make install )
#   ============================================================================
#ln -sf      "${NmDir}/nm-files/mod/SciTEGlobal.properties"                      \
#            "${SlsRootDir}/src/SciTEGlobal.properties"


exit 0




