#!/bin/bash
#   ############################################################################
CbSvnRevision="$1"

CbSvnDir="svn${CbSvnRevision}"
CbSvnArchive="svn${CbSvnRevision}.tar.bz2"

echo "> cleaning directory [${CbSvnDir}]"

if [[ ! -e "${CbSvnDir}" ]] ; then

    echo "> svn directory does not exist, exiting..."
    exit 0

fi

rm -rf "${CbSvnDir}"

#   ############################################################################
echo "> extracting from [${CbSvnArchive}]"

cat "${CbSvnArchive}" | bunzip2 | tar -x

echo "> done"


exit 0
