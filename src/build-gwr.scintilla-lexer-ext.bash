#!/bin/bash

#   ############################################################################
#   src/build-gwr.scintilla-lexer-ext.bash
#
#   ############################################################################

#   ############################################################################
#   INCLUDES
#   ############################################################################
source  /usr/local/bin/gwr.cb-build-helper.include.bash
#   ############################################################################
#   VARIABLES
#   ############################################################################
NmDir="${SlcRootDir}/src/${SlcCbVersion}"
SleFileD=""
SleFileN=""
SleFileR=""
#   ############################################################################
#   FUNCTIONS
#   ############################################################################

#   ############################################################################
#   MAIN
#   ############################################################################
#   ============================================================================
#   describe & import new/mod files from codeblocks::gwr.Scintilla-LexerExt
#   ============================================================================
cbh_func__log "> describe & import new/mod files from codeblocks::Scintilla-LexerExt"

cbh_func__df_import     "${NmDir}" "${SlcRootDir}/src/gwr.Scintilla-LexerExt"
cbh_func__check_errors  $?
#   ============================================================================
#   describe & import new/mod files from Scintilla-LexerExt -
#   ============================================================================
cbh_func__log "> describe & import new/mod files from Scintilla-LexerExt"
#   ----------------------------------------------------------------------------
#   import & describe files : LexExt.cxx
#   ----------------------------------------------------------------------------
#   LextExt.cxx
SleFileD="${SleLEDir}"
SleFileN="LexExt.cxx"
SleFileR="src/sdk/wxscintilla/src/scintilla/lexers"                             #   ref dir will be "trunk"

cbh_func__df_add_new    "${NmDir}" "${SleFileD}" "${SleFileN}" "${SleFileR}" "nolink"
cbh_func__check_errors  $?
#   ----------------------------------------------------------------------------
#   - describe & import : languages properties files
#   - modify  LexerModule-s linkage in LexExt.cxx with the intended mechanism
#   - set lexer id in lexer_language.xml files
#   - update language-dependant links that system-links point on ( if no make install )
#   ----------------------------------------------------------------------------
SleFileR="src/sdk/resources/lexers"

for language in ${SlePLs} ; do

    ulanguage="${language//-/_}"

    SleFileD="${SlePLDir}/${language}/properties/codeblocks"
    SleFileN="lexer_ext__${ulanguage}.xml"
    cbh_func__df_add_new    "${NmDir}" "${SleFileD}" "${SleFileN}" "${SleFileR}" "nolink"
    cbh_func__check_errors  $?

    #   set lexer id
    sed -i.bak "s/_?_index_?_/${SlcCbLexerId}/" "${NmDir}/nm-files/new/lexer_ext__${ulanguage}.xml"
    #sed -i.bak "s/_?_index_?_/TOTO" "${NmDir}/nm-files/new/lexer_ext__${ulanguage}.xml"

    #   update language-dependant link that system-link point on ( if no make install )
    ln -sf      "${NmDir}/nm-files/new/${SleFileN}"                             \
                "${SlcRootDir}/src/${SleFileN}"
    #  .........................................................................
    SleFileD="${SlePLDir}/${language}/properties/codeblocks"
    SleFileN="lexer_ext__${ulanguage}.sample"
    cbh_func__df_add_new    "${NmDir}" "${SleFileD}" "${SleFileN}" "${SleFileR}"
    cbh_func__check_errors  $?

    #   update language-dependant link that system-link point on ( if no make install )
    ln -sf      "${NmDir}/nm-files/new/${SleFileN}"                             \
                "${SlcRootDir}/src/${SleFileN}"

    #   no linkage preparation, one LexerModule only

done

linkage="\/\/  linkage"
linkage="${linkage}\n"
linkage="${linkage}LexerModule lmExt(SCLEX_EXT, LexerExt::LexerFactoryExt, \"ext\", extWordListDesc);"
sed -i.bak "s/SCITE_OR_CODEBLOCKS_MODULE_LINKAGE/${linkage}/" "${NmDir}/nm-files/new/LexExt.cxx"

exit 0
