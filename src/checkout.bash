#!/bin/bash
#   ############################################################################
echo "> getting last revision number..."

rm -f checkout.01.txt
rm -f index.html
wget -q "http://svn.code.sf.net/p/codeblocks/code/trunk/"

cat index.html | sed -ne '1p' > checkout.01.txt

SvnRevision=$( cat checkout.01.txt | sed -ne 's/\(.*Revision \)\([0-9]*\)\(.*\)/\2/p' )

SvnRevision=9857

SvnDirectory="svn${SvnRevision}"

echo "> svn version   :[${SvnRevision}]"
echo "> svn directory :[${SvnDirectory}]"

if [[ ! -e "${SvnDirectory}" ]] ; then

    echo "> svn directory does not exist, creating..."
    mkdir "${SvnDirectory}"

fi

#   ############################################################################
echo "> checkout..."

cd "${SvnDirectory}"

SvnCommand="svn checkout svn://svn.code.sf.net/p/codeblocks/code/trunk@${SvnRevision}"
echo "  command [${SvnCommand}]"
eval "${SvnCommand}"

cd ../


exit 0
