/*
 * This file is part of the Code::Blocks IDE and licensed under the GNU General Public License, version 3
 * http://www.gnu.org/licenses/gpl-3.0.html
 */

#ifndef OPENFILESLISTPLUGINPANEL_H
#define OPENFILESLISTPLUGINPANEL_H
//  ............................................................................
#include    <wx/event.h>
//  ............................................................................
class   OpenFilesListPlugin;
class   OpenFilesListPluginOptionsMenu;
class   OpenFilesListPluginPanelHeader;
class   OpenFilesListPluginPanel;
class   OpenFilesListPluginPanelItemData;
class   OpenFilesListPluginPanelItemInfo;
class   EditorBase;

typedef wxPanel                             OFLPCtrl;
typedef OpenFilesListPluginOptionsMenu      OFLPOptionsMenu;
typedef OpenFilesListPluginPanel            OFLPPanel;
typedef OpenFilesListPluginPanelItemData    OFLPPanelItemData;
typedef OpenFilesListPluginPanelItemInfo    OFLPPanelItemInfo;
typedef OpenFilesListPluginPanelHeader      OFLPPanelHeader;

WX_DEFINE_ARRAY(EditorBase              *, EditorArray);
WX_DEFINE_ARRAY(OpenFilesListPluginPanel*, PanelArray);

/// ****************************************************************************
//! \class  OpenFilesListPluginOptionsMenu
//!
//! \brief  Data stored in all tree items of panels
//!
//! \detail Points on the OFLPPanel, and the EditorBase
/// ****************************************************************************
class OpenFilesListPluginOptionsMenu        : public wxMenu
{
  private:
    wxMenu      *   dw_om_mode;

  public:
    wxMenu      *   get_menu_mode() { return dw_om_mode;   }

  public:
    OpenFilesListPluginOptionsMenu();
};
/// ****************************************************************************
//! \class  OpenFilesListPluginPanelItemData
//!
//! \brief  Data stored in all tree items of panels
//!
//! \detail Points on the OFLPPanel, and the EditorBase
/// ****************************************************************************
class OpenFilesListPluginPanelItemData      : public wxTreeItemData
{
  public:
    OpenFilesListPluginPanelItemData(
        OpenFilesListPluginPanel    *   _panel  ,
        EditorBase                  *   _ed     );

  protected:
    EditorBase                  *   a_editor;
    OpenFilesListPluginPanel    *   a_panel;

  protected:
    virtual void                    p1_raz()    { a_editor = NULL; a_panel = NULL; }

  public:
    EditorBase                  *   GetEditor() const   { return a_editor;  }
    OpenFilesListPluginPanel    *   GetPanel()  const   { return a_panel;   }

};
/// ****************************************************************************
//! \class  OpenFilesListPluginPanelItemInfo
//!
//! \brief  Data stored in all tree items of panels + other datas  ; construct
//!     only from a wxTeeeEvent : its for event handling coding lisibility
//!
//! \detail Points on the OFLPPanel, the EditorBase, has the wxTeeeItemId, and
//!     the wxTreeCtrl
/// ****************************************************************************
class OpenFilesListPluginPanelItemInfo      : public OpenFilesListPluginPanelItemData
{
  public:
    OpenFilesListPluginPanelItemInfo(wxTreeEvent& _e);

  private:
    bool                            a_is_ok;
    wxTreeCtrl                  *   a_tree;
    wxTreeItemId                    a_iid;

  protected:
    virtual void                    p1_raz()    { a_tree = NULL; a_iid.Unset(); }

  public:
    bool                            IsOk()      { return a_is_ok;   }
    wxTreeCtrl                  *   GetTree()   { return a_tree;    }
    wxTreeItemId                    GetIid()    { return a_iid;     }
};
/// ****************************************************************************
//! \class  OFLPPanelDataObject
//!
//! \brief  Allow parameters sharing between DnD src and dst widgets
//!
//! \detail We inherit from wxDataObjectSimple, and serialize / deserialize some
//!     values.
/// ****************************************************************************
class OFLPPanelDataObject                   : public wxDataObjectSimple
{
  public:
    OFLPPanelDataObject();

  private:
    static  const   size_t  s_capacity          =   64;
    static  const   size_t  s_panel_index_max   =   255;
    static  const   size_t  s_item_index_max    =   255;

    //  serialized data for dnd
    char    a_data[s_capacity];
    size_t  a_data_size;                                                        //! a_data_size = 0 <=> no data stored in object

    //  ( wxWidgets would say ) "formatted" data for OFLPPanel
    unsigned    char    a_data_panel_index;                                     //! formatted OFLPPanel index
    unsigned    char    a_data_item_index;                                      //! formatted item index in above OFLPPanel
    EditorBase  *       a_data_editor;                                          //! formatted EditorBase*

  private:
    bool                p0_serialize();
    bool                p0_deserialize();
    EditorBase  **      p0_serialized_editor_pointer()
        {
            return  (EditorBase**)( a_data + 2 );
        }

  public:
    virtual size_t  GetDataSize ()                              const;
    virtual bool    GetDataHere (void *buf)                     const;
    virtual bool    SetData     (size_t len, const void *buf);

            int             GetDataCapacity ()  { return s_capacity; }
            int             GetDataPanelIx  ();
            int             GetDataItemIx   ();
            EditorBase  *   GetDataEditor   ();
            bool            SetData         (int _panel_ix, int _item_ix, EditorBase*);
            void            ClrData         ()  { a_data_size = 0; }
};
/// ****************************************************************************
//! \class  OpenFilesListPluginPanelDropTarget
//!
//! \brief  For Dnd
/// ****************************************************************************
class OpenFilesListPluginPanelDropTarget    : public wxDropTarget
{
  private:
    wxTreeCtrl              *   a_owner;
    OFLPPanel               *   a_owner_panel;
    OFLPPanelDataObject     *   d_data_object;

  public:
    wxTreeCtrl              *   octrl()     { return a_owner;       }
    OFLPPanel               *   opanel()    { return a_owner_panel; }

  public:
    virtual bool            OnDrop(wxCoord x, wxCoord y);
    virtual wxDragResult    OnData(wxCoord x, wxCoord y, wxDragResult _res);

  public:
    OpenFilesListPluginPanelDropTarget(wxTreeCtrl *_owner, OFLPPanel* _owner_panel);
   ~OpenFilesListPluginPanelDropTarget();
};
/// ****************************************************************************
//! \class  OpenFilesListPluginPanelHeader
//!
//! \brief  Header widget
//!
//! \detail Header owns title and buttons, but does not create buttons by
//!     itself.
/// ****************************************************************************
class OpenFilesListPluginPanelHeader        : public wxPanel
{
    friend class OpenFilesListPluginPanel;

  private:
    WX_DEFINE_ARRAY(wxButton*, ButtonsArray);

  private:
    OFLPPanel           *       aw_parent;                                      //  _GWR_TODO_ can use this->GetParent() instead

    wxString                    a_title;

    wxPanel             *       dw_panel;
    wxSizer             *       dw_sizer;
    wxWindow            *           aw_txt;
    wxWindow            *           dw_txt_old;
    wxStaticText        *           dw_txt_sta;
    wxTextCtrl          *           dw_txt_dyn;

    ButtonsArray                    a_buttons_array;

  public:
    wxString    const   &           get_title()                 const   { return a_title; }

  private:
    void                    p0_create_buttons       ();
    bool                    p0_title_ctrl_replace   (wxWindow* _wnew);
    //  ------------------------------------------------------------------------
  public:
    void                    title_switch_to_dynamic   ();
    void                    title_switch_to_static    ();

    wxButton            *   button          (int _ix);
    void                    button_prepend  (int _bitmap_id);
    void                    button_append   (int _bitmap_id);
    void                    button_show     (int _ix, bool);

    //  ------------------------------------------------------------------------
  public:
    OpenFilesListPluginPanelHeader();
    OpenFilesListPluginPanelHeader(OFLPPanel* _parent, wxString _title);
    virtual ~OpenFilesListPluginPanelHeader();
};
/// ****************************************************************************
//! \class  OpenFilesListPluginPanel
//!
//! \brief  wxPanel containing a OpenFilesListPluginPanelHeader and a wxTreeCtrl
/// ****************************************************************************
class OpenFilesListPluginPanel              : public wxPanel
{
  protected:
    OpenFilesListPlugin *   a_ofl_plugin;

    bool                    a_bulk;

    //bool                    a_minimized;

    bool                    a_allow_kill_focus_event;

    OFLPPanelHeader     *   dw_header;
    wxTreeCtrl          *   d_tree;
    wxBoxSizer          *   dw_sizer;

    EditorArray             a_editors_array;                                    //  speed up finding editors

    OpenFilesListPluginPanelDropTarget  *   d_drop_target;
    //  ------------------------------------------------------------------------
  public:
    OpenFilesListPlugin *   const   plugin()                    const   { return a_ofl_plugin;          }
    bool                            is_bulk()                   const   { return a_bulk;                }
    EditorArray const   *           get_editors()               const   { return &a_editors_array;      }
    wxTreeCtrl  const   *   const   tree()                      const   { return d_tree;                }
    wxString    const   &           get_title()                 const   { return dw_header->get_title();}
    //  ------------------------------------------------------------------------
    static      wxString            stringize_drag_result(wxDragResult _dres);
    //  ------------------------------------------------------------------------
  protected:
    void                    p0_create_tree      ();

    wxTreeItemId            item_append         (EditorBase*);
    wxTreeItemId            item_find           (EditorBase*);
    bool                    item_del            (EditorBase*);
    bool                    item_select         (EditorBase*);

    void                    items_deselect      ();
    void                    items_del           ();
    //  ------------------------------------------------------------------------
  public:
    void                    reset();

    int                     editor_index    (EditorBase*);
    wxTreeItemId            editor_add      (EditorBase*);
    void                    editor_del      (EditorBase*);
    void                    editor_select   (EditorBase*);
    void                    editor_sync     (EditorBase*);

    void                    editors_del     ();
    void                    editors_deselect();

    bool                    is_minimized();
    void                    minimize();
    void                    maximize();
    //  ------------------------------------------------------------------------
  protected:
    void                    OnSelect    (wxCommandEvent&);
    void                    OnDragInit  (wxTreeEvent&);
    void                    OnDragEnd   (wxTreeEvent&);
    //  ------------------------------------------------------------------------
  private:
    void                    p0_allow_kill_focus_event               (bool);
  public:
    void                    evh_title_static_LEFT_DOWN              (wxMouseEvent   &);
    void                    evh_title_dynamic_TEXT_ENTER            (wxCommandEvent &);
    void                    evh_title_dynamic_KILL_FOCUS            (wxFocusEvent   &);
    //  ------------------------------------------------------------------------
  public:
    OpenFilesListPluginPanel();
    OpenFilesListPluginPanel(OpenFilesListPlugin* _ofl_plugin, wxWindow* _parent, wxString _title, bool _bulk = false);
    virtual ~OpenFilesListPluginPanel();
};
/// ****************************************************************************
//! \class  OpenFilesListPluginPanelBulk
//!
//! \brief  Like OpenFilesListPluginPanel, but not deletable, not renamable,
//!     and containg the options menu
/// ****************************************************************************
class OpenFilesListPluginPanelBulk          : public OpenFilesListPluginPanel
{
  private:
    wxMenu      *   dw_menu_main;

    wxMenu      *       dw_menu_selection_mode;
    wxMenuItem  *           dw_item_selection_standard;
    wxMenuItem  *           dw_item_selection_productivity;

    //  ------------------------------------------------------------------------
  public:
    OpenFilesListPluginPanelBulk(OpenFilesListPlugin* _ofl_plugin, wxWindow* _parent, wxString _title);
    virtual ~OpenFilesListPluginPanelBulk();
};

#endif                                                                          // OPENFILESLISTPLUGINPANEL_H

