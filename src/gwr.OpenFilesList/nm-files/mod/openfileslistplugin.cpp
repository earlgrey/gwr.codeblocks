/*
 * This file is part of the Code::Blocks IDE and licensed under the GNU General Public License, version 3
 * http://www.gnu.org/licenses/gpl-3.0.html
 *
 * $Revision: 10319 $
 * $Id: openfileslistplugin.cpp 10319 2015-06-03 23:03:31Z fuscated $
 * $HeadURL: svn://svn.code.sf.net/p/codeblocks/code/trunk/src/plugins/openfileslist/openfileslistplugin.cpp $
 */

#include "sdk.h"

#ifndef CB_PRECOMP
    #include <manager.h>
    #include <configmanager.h>
    #include <editormanager.h>
    #include <projectmanager.h>
    #include <logmanager.h>
    #include <editorbase.h>
    #include <sdk_events.h>
    #include <misctreeitemdata.h>

    #include <wx/window.h>
    #include <wx/treectrl.h>
    #include <wx/bitmap.h>
    #include <wx/imaglist.h>
    #include <wx/menu.h>
#endif

#include    "openfileslistplugin.h"
//  ............................................................................
#include    <wx/event.h>
//  ............................................................................
#define GWR_OFLP_SANITY_CHECKS

namespace
{

wxLogWindow *   s_logger    = NULL;

}

//#define     GWR_DEBUG_PRINTF
#define     GWR_DEBUG_WXLOG
#include    "gwrlogger.h"
//  ............................................................................
static  int GetOpenFilesListIcon(EditorBase* ed);

namespace
{
    // this auto-registers the plugin
    PluginRegistrant<OpenFilesListPlugin> reg(_T("OpenFilesList"));

    const int idOpenFilesTree           = wxNewId();
    const int idViewOpenFilesTree       = wxNewId();
    const int idMainPanel               = wxNewId();



    class OpenFilesListData : public wxTreeItemData
    {
        public:
            OpenFilesListData(EditorBase* ed_in) : ed(ed_in) {}
            EditorBase* GetEditor() const { return ed; }
        private:
            EditorBase* ed;
    };
}

BEGIN_EVENT_TABLE(OpenFilesListPlugin, cbPlugin)
    EVT_UPDATE_UI               (idViewOpenFilesTree, OpenFilesListPlugin::OnUpdateUI)
    EVT_MENU                    (idViewOpenFilesTree, OpenFilesListPlugin::OnViewOpenFilesTree)
    //EVT_TREE_ITEM_ACTIVATED     (idOpenFilesTree    , OpenFilesListPlugin::OnTreeItemActivated)
    //EVT_TREE_ITEM_RIGHT_CLICK   (idOpenFilesTree    , OpenFilesListPlugin::OnTreeItemRightClick)
    //EVT_TREE_SEL_CHANGED        (idOpenFilesTree    , OpenFilesListPlugin::OnTreeSelChanged)
END_EVENT_TABLE()

OpenFilesListPlugin::OpenFilesListPlugin()
    :   a_option_mode(OPT_MODE_STANDARD)
{
}

OpenFilesListPlugin::~OpenFilesListPlugin()
{
    //dtor
}

void OpenFilesListPlugin::OnAttach()
{
    GWR_INF("OpenFilesListPlugin::OnAttach [%08x]", this);

    m_ViewMenu = 0;
    m_EditorArray.Clear();
    a_panels_array.Clear();

    #ifdef  GWR_DEBUG_WXLOG
    wxLog::EnableLogging(true);
    s_logger = new wxLogWindow(Manager::Get()->GetAppWindow(), _T(" OpenFilesList"), true, false);
    wxFont fnt(8, wxFONTFAMILY_TELETYPE, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxString::FromUTF8("monospace") );
    wxWindow* www = NULL;
    wxWindowList wl = s_logger->GetFrame()->GetChildren();
    for ( wxWindowList::iterator it = wl.begin() ; it != wl.end() ; it++ )
    {
        www=*it;
        www->SetFont(fnt);
    }
    wxLog::SetActiveTarget(s_logger);
    s_logger->Flush();
    s_logger->GetFrame()->SetSize(20,30,600,300);
    wxLogMessage( _T("---OpenFilesList Plugin Logging Started ---"));
    #endif

    // load bitmaps
    wxBitmap bmp;
    //m_pImages = new wxImageList(16, 16);
    wxString prefix = ConfigManager::GetDataFolder() + _T("/images/");

    bmp = cbLoadBitmap(prefix + _T("folder_open.png"), wxBITMAP_TYPE_PNG);      // 0 folder
    GetImagesList()->Add(bmp);

    bmp = cbLoadBitmap(prefix + _T("ascii.png"), wxBITMAP_TYPE_PNG);            // 1 file
    GetImagesList()->Add(bmp);

    bmp = cbLoadBitmap(prefix + _T("modified_file.png"), wxBITMAP_TYPE_PNG);    // 2 modified file
    GetImagesList()->Add(bmp);

    bmp = cbLoadBitmap(prefix + _T("file-readonly.png"), wxBITMAP_TYPE_PNG);    // 3 read only file
    GetImagesList()->Add(bmp);
    //  ------------------------------------------------------------------GWR >>
    bmp = cbLoadBitmap(prefix + _T("ofl-panel-header-button-bar-green.png") , wxBITMAP_TYPE_PNG);    // 4 add panel
    GetImagesList()->Add(bmp);
    bmp = cbLoadBitmap(prefix + _T("ofl-panel-header-button-bar-red.png")   , wxBITMAP_TYPE_PNG);    // 5 del panel
    GetImagesList()->Add(bmp);
    bmp = cbLoadBitmap(prefix + _T("ofl-panel-header-button-bar-blue.png")  , wxBITMAP_TYPE_PNG);    // 6 options
    GetImagesList()->Add(bmp);
    bmp = cbLoadBitmap(prefix + _T("ofl-panel-header-button-bar-up.png")    , wxBITMAP_TYPE_PNG);    // 7 move panel up
    GetImagesList()->Add(bmp);
    bmp = cbLoadBitmap(prefix + _T("ofl-panel-header-button-bar-down.png")  , wxBITMAP_TYPE_PNG);    // 8 move panel down
    GetImagesList()->Add(bmp);
    bmp = cbLoadBitmap(prefix + _T("ofl-panel-header-button-bar-orange.png"), wxBITMAP_TYPE_PNG);    // 9 (mini | maxi) mize panel
    GetImagesList()->Add(bmp);

    //  create main wxPanel & its sizer, "bulk" panel
    d_MainPanel =   new wxPanel(Manager::Get()->GetAppWindow(), idMainPanel);
    d_MainSizer =   new wxBoxSizer(wxVERTICAL);
    d_BulkPanel =   AddPanel( wxString::FromUTF8("bulk"), true );

    d_MainSizer->SetSizeHints(d_MainPanel);
    d_MainPanel->SetSizer(d_MainSizer);

    dw_options_menu =   new OpenFilesListPluginOptionsMenu();

    GetOptionsMenu()->get_menu_mode()->Connect(
        wxEVT_COMMAND_MENU_SELECTED                                         ,
        wxCommandEventHandler(OpenFilesListPlugin::evh_menu_option_checked) ,
        NULL, this                                                          );
    //  ------------------------------------------------------------------GWR <<
    // first build of the tree
    ////RebuildOpenFilesTree();

    // add the tree to the docking system
    CodeBlocksDockEvent evt(cbEVT_ADD_DOCK_WINDOW);
    evt.name        = _T("OpenFilesPane");
    evt.title       = _("Open files list");
    evt.pWindow     = d_MainPanel;
    evt.minimumSize.Set(50, 50);
    evt.desiredSize.Set(150, 100);
    evt.floatingSize.Set(100, 150);
    evt.dockSide    = CodeBlocksDockEvent::dsLeft;
    evt.stretch     = true;
    Manager::Get()->ProcessEvent(evt);

    // register event sinks
    Manager* pm = Manager::Get();

    // basically everything editor related
    //pm->RegisterEventSink(cbEVT_PROJECT_OPEN, new cbEventFunctor<OpenFilesListPlugin, CodeBlocksEvent>(this, &OpenFilesListPlugin::OnProjectOpened));
    //pm->RegisterEventSink(cbEVT_PROJECT_ACTIVATE, new cbEventFunctor<OpenFilesListPlugin, CodeBlocksEvent>(this, &OpenFilesListPlugin::OnProjectOpened));
    pm->RegisterEventSink(cbEVT_EDITOR_OPEN, new cbEventFunctor<OpenFilesListPlugin, CodeBlocksEvent>(this, &OpenFilesListPlugin::OnEditorOpened));
    pm->RegisterEventSink(cbEVT_EDITOR_CLOSE, new cbEventFunctor<OpenFilesListPlugin, CodeBlocksEvent>(this, &OpenFilesListPlugin::OnEditorClosed));
    pm->RegisterEventSink(cbEVT_EDITOR_ACTIVATED, new cbEventFunctor<OpenFilesListPlugin, CodeBlocksEvent>(this, &OpenFilesListPlugin::OnEditorActivated));
    pm->RegisterEventSink(cbEVT_EDITOR_MODIFIED, new cbEventFunctor<OpenFilesListPlugin, CodeBlocksEvent>(this, &OpenFilesListPlugin::OnEditorModified));
    pm->RegisterEventSink(cbEVT_EDITOR_SAVE, new cbEventFunctor<OpenFilesListPlugin, CodeBlocksEvent>(this, &OpenFilesListPlugin::OnEditorSaved));
    //pm->RegisterEventSink(cbEVT_EDITOR_DEACTIVATED, new cbEventFunctor<OpenFilesListPlugin, CodeBlocksEvent>(this, &OpenFilesListPlugin::OnEditorDeactivated));
}

void OpenFilesListPlugin::OnRelease(bool appShutDown)
{
    if (appShutDown)
        return;
    // remove registered event sinks
    Manager::Get()->RemoveAllEventSinksFor(this);

    // remove tree from docking system
    CodeBlocksDockEvent evt(cbEVT_REMOVE_DOCK_WINDOW);
    evt.pWindow = d_MainPanel;
    Manager::Get()->ProcessEvent(evt);

    // finally destroy the tree
    d_MainPanel->Destroy();
    d_MainPanel = nullptr;
}

//  c::b "View" submenu                                                         //  _GWR_NOTE_
void OpenFilesListPlugin::BuildMenu(wxMenuBar* menuBar)
{
    // if not attached, exit
    if (!IsAttached())
        return;

    // add the open files list in the "View" menu
    int idx = menuBar->FindMenu(_("&View"));
    if (idx != wxNOT_FOUND)
    {
        m_ViewMenu = menuBar->GetMenu(idx);
        wxMenuItemList& items = m_ViewMenu->GetMenuItems();
        // find the first separator and insert before it
        for (size_t i = 0; i < items.GetCount(); ++i)
        {
            if (items[i]->IsSeparator())
            {
                m_ViewMenu->InsertCheckItem(i, idViewOpenFilesTree, _("&Open files list"), _("Toggle displaying the open files list"));
                return;
            }
        }
        // not found, just append
        m_ViewMenu->AppendCheckItem(idViewOpenFilesTree, _("&Open files list"), _("Toggle displaying the open files list"));
    }
}

/*
void OpenFilesListPlugin::RebuildOpenFilesTree()
{
    GWR_INF("%s", _T("OpenFilesListPlugin::RebuildOpenFilesTree()"));
    //  ........................................................................
    if (Manager::IsAppShuttingDown())
        return;

    return;

    EditorManager* mgr = Manager::Get()->GetEditorManager();

    //d_MainPanel->Freeze();

    ResetPanels();

    if (!mgr->GetEditorsCount())
    {
        //d_MainPanel->Thaw();
        return;
    }

    // loop all open editors
    for (int i = 0; i < mgr->GetEditorsCount(); ++i)
    {
        EditorBase* ed = mgr->GetEditor(i);
        if (!ed || !ed->VisibleToTree())
            continue;

        d_BulkPanel->editor_add(ed);
        ////if (mgr->GetActiveEditor() == ed)
            ////m_pTree->SelectItem(item);
    }

    ////m_pTree->SortChildren(m_pTree->GetRootItem());
    //d_MainPanel->Thaw();
}
*/
//! \brief  RefreshOpenFilesTree
//!
//! \detail Given an editor :
//!     if have to remove : remove it
//!     else
//!       - if    editor in list  : ensure corresponding wxTreeItem is selected
//!         else                  : add a new wxTreeItem
//!       - verify wxTreeItem icon
//!       - verify wxTreeItem text
void OpenFilesListPlugin::RefreshOpenFilesTree(EditorBase* ed, bool remove)
{
    OFLPPanel                       *   panel   =   NULL;
    wxTreeItemId                    iid;
    //  ........................................................................
    GWR_INF("%s", _T("OpenFilesListPlugin::RefreshOpenFilesTree()"));
    //  ........................................................................
    if (Manager::IsAppShuttingDown() || !ed)
        return;

    EditorManager   * mgr   =   Manager::Get()->GetEditorManager();
    EditorBase      * aed   =   mgr->GetActiveEditor();

    d_MainPanel->Freeze();

    wxTreeItemIdValue   cookie      = 0;
    wxString            shortname   = ed->GetShortName();

    //  find panel from editor
    panel = GetPanel(ed);
    if ( panel )
    {
        if ( !remove )
        {
            //  ensure visual infos are correct ( icon, name + eventually re-sorting )
            panel->editor_sync(ed);

            //  ensure selected if it is the active one
            if ( ed == aed )
            {
                panel->editor_select(ed);
                DeselectItemsOfAllPanelsButOne(panel);
            }
        }
        else
        {
            panel->editor_del(ed);
        }
    }
    else
    {
        // not found, not to-remove and valid name: add new editor
        if ( !remove && ed->VisibleToTree() && !shortname.IsEmpty())
        {
            d_BulkPanel->editor_add(ed);
        }
    }
    d_MainPanel->Thaw();
}

// view menu toggle tree
void OpenFilesListPlugin::OnViewOpenFilesTree(wxCommandEvent& event)
{
    CodeBlocksDockEvent evt(event.IsChecked() ? cbEVT_SHOW_DOCK_WINDOW : cbEVT_HIDE_DOCK_WINDOW);
    evt.pWindow = d_MainPanel;
    Manager::Get()->ProcessEvent(evt);
}

void OpenFilesListPlugin::OnUpdateUI(wxUpdateUIEvent& event)
{
    if (m_ViewMenu)
    {
        bool isVis = IsWindowReallyShown((wxWindow*)d_MainPanel);
        m_ViewMenu->Check(idViewOpenFilesTree, isVis);
    }

    // must do...
    event.Skip();
}
//  ############################################################################
void OpenFilesListPlugin::OnProjectOpened(cb_unused CodeBlocksEvent& event)
{
    GWR_INF("%s", _T("########## OpenFilesListPlugin::OnProjectOpened() ##########\n"));
    //  ........................................................................
    /*
    if (m_EditorArray.IsEmpty())
        return;

    for (size_t i = 0; i < m_EditorArray.GetCount(); ++i)
    {
        EditorBase* eb = m_EditorArray[i];
        if (eb)
        {
            RefreshOpenFilesTree(eb);
        }
    }
    m_EditorArray.Clear();
    */
}

void OpenFilesListPlugin::OnEditorOpened(CodeBlocksEvent& event)
{
    GWR_INF("%s", _T("########## OpenFilesListPlugin::OnEditorOpened() ##########\n"));
    //  ........................................................................
    EditorBase  *   eb  =   event.GetEditor();
    //  ........................................................................
    if (Manager::Get()->GetProjectManager()->IsBusy() && eb && ( GetPanel(eb) == NULL) )
    {
        d_BulkPanel->editor_add(eb);
    }
    else
    {
        RefreshOpenFilesTree(eb);
    }
    /*
    EditorBase* eb = event.GetEditor();
    if (Manager::Get()->GetProjectManager()->IsBusy() && eb && (m_EditorArray.Index(eb) == wxNOT_FOUND))
    {
        m_EditorArray.Add(eb);
    }
    else
    {
        RefreshOpenFilesTree(eb);
    }
    */
}

void OpenFilesListPlugin::OnEditorClosed(CodeBlocksEvent& event)
{
    GWR_INF("%s", _T("########## OpenFilesListPlugin::OnEditorClosed() ##########"));
    RefreshOpenFilesTree(event.GetEditor(), true);
}

void OpenFilesListPlugin::OnEditorActivated(CodeBlocksEvent& event)             //  _GER_REM_   editors may be activated by tabs
{
    GWR_INF("########## OpenFilesListPlugin::OnEditorActivated(%s) ##########", event.GetEditor()->GetShortName().c_str());
    //  ........................................................................
    //  Manager::Get()->GetLogManager()->Log(_T("OnEditorActivated: ") + event.GetEditor()->GetFilename());
    RefreshOpenFilesTree(event.GetEditor());
}

void OpenFilesListPlugin::OnEditorDeactivated(CodeBlocksEvent& event)
{
//  Manager::Get()->GetLogManager()->Log(_T("OnEditorDeactivated: ") + event.GetEditor()->GetFilename());
    RefreshOpenFilesTree(event.GetEditor());
}

void OpenFilesListPlugin::OnEditorModified(CodeBlocksEvent& event)
{
    GWR_INF("%s", _T("########## OpenFilesListPlugin::OnEditorModified() ##########"));
    RefreshOpenFilesTree(event.GetEditor());
}

void OpenFilesListPlugin::OnEditorSaved(CodeBlocksEvent& event)
{
    GWR_INF("%s", _T("########## OpenFilesListPlugin::OnEditorSaved() ##########"));
    RefreshOpenFilesTree(event.GetEditor());
}
//  ############################################################################
// tree item double-clicked
void OpenFilesListPlugin::OnTreeItemActivated(wxTreeEvent& event)
{
    OFLPPanelItemInfo   inf( event );
    //  ........................................................................
    GWR_INF("%s", _T("########## OpenFilesListPlugin::OnTreeItemActivated() ##########"));

    if (Manager::IsAppShuttingDown())
        return;

    if ( ! inf.IsOk() )
        return;

    //  In productivity mode, selecting a file :
    //  - makes the corresponding editor the active editor ; since activation in
    //  wxWidgets correspond to a double click, that editor has already been
    //  made the active one.
    //  - reload the file
    if ( mode_productivity() )
    {
        //  reload the file
        Manager::Get()->GetEditorManager()->GetBuiltinEditor(inf.GetEditor())->Reload();
    }
    //  In productivity mode, selecting a file :
    //  - makes the corresponding editor the active editor
    if ( mode_standard() )
    {
        //  active selected editor
        Manager::Get()->GetEditorManager()->SetActiveEditor(inf.GetEditor());
    }
}

// tree item right-clicked
void OpenFilesListPlugin::OnTreeItemRightClick(wxTreeEvent& event)
{
    OFLPPanelItemInfo   inf( event );
    //  ........................................................................
    GWR_INF("%s", _T("########## OpenFilesListPlugin::OnTreeItemRightClick() ##########"));

    if (Manager::IsAppShuttingDown())
        return;

    if ( ! inf.IsOk() )
        return;

    wxPoint pt = inf.GetTree()->ClientToScreen(event.GetPoint());
    inf.GetEditor()->DisplayContextMenu(pt,mtOpenFilesList);
}

// tree sel changed
void OpenFilesListPlugin::OnTreeSelChanged(wxTreeEvent &event)
{
    GWR_INF("%s", _T("########## OpenFilesListPlugin::OnTreeSelChanged() ##########"));
    //  ........................................................................
    OFLPPanelItemInfo   inf( event );
    //  ........................................................................

    if(Manager::IsAppShuttingDown())
        return;

    if ( ! inf.IsOk() )
        return;

    //  in productivity mode, selecting a file makes the corresponding editor
    //  the active editor
    if ( mode_productivity() )
    {
        //  active selected editor
        Manager::Get()->GetEditorManager()->SetActiveEditor(inf.GetEditor());
    }

    //  deselect any other wxTreeCtrl's selected item
    DeselectItemsOfAllPanelsButOne(inf.GetPanel());
}
//  ############################################################################
//
//                          OPENFILESLISTPLUGIN : ADDITIONS / MODIFICATIONS
//
//  ############################################################################
wxImageList     OpenFilesListPlugin::s_img_list(16,16);

// return the appropriate icon for an editor, based on its state
int             GetOpenFilesListIcon            (EditorBase* ed)
{
    int mod = 1;          // ascii
    if (ed->IsReadOnly())
        mod = 3;          // read only
    else if (ed->GetModified())
        mod =2;           // modified
    return mod;
}

void            OpenFilesListPlugin::ResetPanels            ()
{
    PanelArray::iterator        it;
    OFLPPanel               *   panel   =   NULL;
    PanelArray                  tmp     = a_panels_array;
    //  ........................................................................
    GWR_INF("%s", _T("OpenFilesListPlugin::ResetPanels()"));
    //  ........................................................................
    // loop all panels
    for ( it = tmp.begin() ; it != tmp.end() ; it++ )
    {
        panel = *(it);
        if ( ! panel->is_bulk() )
            DelPanel(panel);
    }
}

int             OpenFilesListPlugin::GetPanelVisualIndex    (OFLPPanel* _panel)
{
    //  wxWindowList is not re-ordered regarding layout changes, so use wxSizer
    //  wxSizer 2.8.12 does not provide GetItemCount(), so use GetChildren()
    //  ........................................................................
    wxSizerItemList                 list;
    wxSizerItemList::iterator       it;
    wxWindow                    *   win     =   NULL;
    int                             index   =   0;
    //  ........................................................................
    #ifdef  GWR_OFLP_SANITY_CHECKS                                              //  _GWR_SANITY_CHECK_
    if ( a_panels_array.Index(_panel) == wxNOT_FOUND )
        return wxNOT_FOUND;
    #endif

    list = d_MainSizer->GetChildren();

    for ( it = list.begin() ; it != list.end() ; it++ )
    {
        win =   (*it)->GetWindow();

        //D GWR_INF("OFLPlugin::GetPanelVisualIndex(%08x):ix[%02i] win[%08x]",
        //D   _panel, index, win );

        if ( win == _panel )
            return index;

        index   =   index   +   1;
    }

    return wxNOT_FOUND;
}

OFLPPanel*      OpenFilesListPlugin::AddPanel               (wxString _title, bool _bulk)
{
    OFLPPanel   *   panel   =   NULL;
    //  ........................................................................
    GWR_INF("%s", _T("OpenFilesListPlugin::AddPanel()"));
    //  ........................................................................
    panel   =   _bulk                                                           ?
                new OpenFilesListPluginPanelBulk( this, d_MainPanel, _title )   :
                new OpenFilesListPluginPanel( this, d_MainPanel, _title )       ;

    d_MainSizer->Add(panel, 1, wxEXPAND, 0);
    d_MainSizer->Layout();

    a_panels_array.Add( panel );

        wxSizerItemList il = d_MainSizer->GetChildren();

        for ( wxSizerItemList::iterator it = il.begin() ; it != il.end() ; it++ )
        {
            GWR_INF("main sizer child [%08x]", *it);
        }

    return panel;
}

void            OpenFilesListPlugin::MovePanelUp            (OFLPPanel* _panel)
{
    size_t  prev_panel_ix   =   0;
    int     pix             =   GetPanelVisualIndex(_panel);
    //  ........................................................................
    GWR_INF("OFLPlugin::MovePanelUp():ix[%i]", pix);
    //  ........................................................................
    #ifdef  GWR_OFLP_SANITY_CHECKS                                              //  _GWR_SANITY_CHECK_
    if ( pix == wxNOT_FOUND )
        return;

    if ( pix <= 1 )
        return;
    #endif

    prev_panel_ix = static_cast< size_t >( pix - 1 );

    d_MainSizer->Detach(_panel);
    d_MainSizer->Insert( prev_panel_ix, _panel, 1, wxEXPAND, 0);
    d_MainSizer->Layout();
}

void            OpenFilesListPlugin::MovePanelDn            (OFLPPanel* _panel)
{
    size_t  next_panel_ix   =   0;
    int     pix             =   GetPanelVisualIndex(_panel);
    //  ........................................................................
    GWR_INF("OFLPlugin::MovePanelDn():ix[%i]", pix);
    //  ........................................................................
    #ifdef  GWR_OFLP_SANITY_CHECKS                                              //  _GWR_SANITY_CHECK_
    if ( pix == wxNOT_FOUND )
        return;
    #endif
    if ( ( 1 + pix ) == a_panels_array.size() )
    {
        GWR_INF("%s", _T("OFLPlugin::MovePanelDn():already the last"));
        return;
    }

    next_panel_ix = static_cast< size_t >( pix + 1 );

    d_MainSizer->Detach(_panel);
    d_MainSizer->Insert( next_panel_ix, _panel, 1, wxEXPAND, 0);
    d_MainSizer->Layout();
}

void            OpenFilesListPlugin::DelPanel               (OFLPPanel* _panel)
{
    EditorArray const   *   array   = _panel->get_editors();
    //  ........................................................................
    for ( EditorArray::const_iterator it = array->begin() ; it != array->end() ; it++)
    {
        d_BulkPanel->editor_add( *it );
    }

    _panel->editors_del();

    d_MainSizer->Detach(_panel);
    d_MainSizer->Layout();

    a_panels_array.Remove(_panel);

    delete _panel;
}

OFLPPanel*      OpenFilesListPlugin::GetPanel               (EditorBase* _editor )
{
    PanelArray::iterator        it;
    OFLPPanel               *   panel   =   NULL;
    //  ........................................................................
    GWR_INF("%s", _T("OpenFilesListPlugin::GetPanel()"));
    //  ........................................................................
    // loop all panels
    for ( it = a_panels_array.begin() ; it != a_panels_array.end() ; it++ )
    {
        panel = *(it);
        if ( panel->editor_index(_editor) != wxNOT_FOUND )
            return panel;
    }
    return NULL;
}

void            OpenFilesListPlugin::DeselectItemsOfAllPanelsButOne(OFLPPanel* _panel)
{
    for ( PanelArray::iterator it = a_panels_array.begin() ; it != a_panels_array.end() ; it++ )
    {
        if ( (*it) != _panel )
            (*it)->editors_deselect();
    }
}
//  ============================================================================
void    OpenFilesListPlugin::   evh_panel_header_button_add_CLICKED (wxCommandEvent& _e)
{
    AddPanel(wxString::FromUTF8("panel"), false);
}

void    OpenFilesListPlugin::   evh_panel_header_button_del_CLICKED (wxCommandEvent& _e)
{
    //  fasten your seatbelt for the cast
    //  event -> wxObject -> wxButton -> GetClientData() -> OFLPPanel*
    OFLPPanel   *   panel   = static_cast< OFLPPanel* >
    (
        ( static_cast< wxButton* >( _e.GetEventObject() ) )->GetClientData()
    );
    //  ........................................................................
    GWR_INF("OFLP::evh_panel_header_button_del_CLICKED():EventObject[%08x] ClientData()[%08x]",
        _e.GetEventObject(), panel);
    //  ........................................................................
    DelPanel(panel);
}

void    OpenFilesListPlugin::   evh_panel_header_button_mm_CLICKED  (wxCommandEvent& _e)
{
    //  fasten your seatbelt for the cast
    //  event -> wxObject -> wxButton -> GetClientData() -> OFLPPanel*
    OFLPPanel   *   panel   = static_cast< OFLPPanel* >
    (
        ( static_cast< wxButton* >( _e.GetEventObject() ) )->GetClientData()
    );
    wxSizerItem *   sitem   = d_MainSizer->GetItem(panel);
    //  ........................................................................
    GWR_INF("OFLP::evh_panel_header_button_sh_CLICKED():EventObject[%08x] ClientData()[%08x] SizerItem[%08x]",
        _e.GetEventObject(), panel, sitem);
    //  ........................................................................
    #ifdef  GWR_OFLP_SANITY_CHECKS                                              //  _GWR_SANITY_CHECK_
    if ( ! sitem )
    {
        GWR_ERR("%s", wxS("  NULL wxSizerItem"));
        return;
    }
    #endif
    //  ........................................................................
    if ( panel->is_minimized() )
    {
        GWR_INF("%s", wxS("  maximizing"));
        panel->maximize();
        sitem->SetProportion(1);
    }
    else
    {
        GWR_INF("%s", wxS("  minimizing"));
        panel->minimize();
        sitem->SetProportion(0);
    }
    d_MainSizer->Layout();
}

void    OpenFilesListPlugin::   evh_panel_header_button_opt_CLICKED (wxCommandEvent& _e)
{
    //  this handler is connect to a wxButton, so
    //  - _e.GetEventObject() is not NULL
    //  - _e.GetEventObject() can be casted to a wxWindow
    static_cast< wxWindow* >( _e.GetEventObject() )->PopupMenu(GetOptionsMenu());
}

void    OpenFilesListPlugin::   evh_panel_header_button_up_CLICKED  (wxCommandEvent& _e)
{
    //  fasten your seatbelt for the cast
    //  event -> wxObject -> wxButton -> GetClientData() -> OFLPPanel*
    OFLPPanel   *   panel = static_cast< OFLPPanel* >
    (
        ( static_cast< wxButton* >( _e.GetEventObject() ) )->GetClientData()
    );
    //  ........................................................................
    GWR_INF("OFLP::evh_panel_header_button_up_CLICKED():EventObject[%08x] ClientData()[%08x]",
        _e.GetEventObject(), panel);

    MovePanelUp(panel);
}

void    OpenFilesListPlugin::   evh_panel_header_button_down_CLICKED(wxCommandEvent& _e)
{
    //  fasten your seatbelt for the cast
    //  event -> wxObject -> wxButton -> GetClientData() -> OFLPPanel*
    OFLPPanel   *   panel = static_cast< OFLPPanel* >
    (
        ( static_cast< wxButton* >( _e.GetEventObject() ) )->GetClientData()
    );
    GWR_INF("OFLP::evh_panel_header_button_dn_CLICKED():EventObject[%08x] ClientData()[%08x]",
        _e.GetEventObject(), panel);

    MovePanelDn(panel);
}

void    OpenFilesListPlugin::   evh_menu_option_checked             (wxCommandEvent& _e)
{
 	switch( _e.GetId() )
    {

    case OpenFilesListPlugin::OPT_MODE_STANDARD     :
    GWR_INF( "%s", wxS("mode standard") );
    a_option_mode   =   OPT_MODE_STANDARD;
    break;

    case OpenFilesListPlugin::OPT_MODE_PRODUCTIVITY :
    GWR_INF( "%s", wxS("mode productivity") );
    a_option_mode   =   OPT_MODE_PRODUCTIVITY;
    break;

 	}
}
//  ############################################################################
//
//                          OFLPPanelPluginOptionsMenu
//
//  ############################################################################
OpenFilesListPluginOptionsMenu::OpenFilesListPluginOptionsMenu()
{
    dw_om_mode = new wxMenu();
        dw_om_mode->AppendRadioItem(OpenFilesListPlugin::OPT_MODE_STANDARD    , wxS("Standard")       , _("Edit files by double-click"));
        dw_om_mode->AppendRadioItem(OpenFilesListPlugin::OPT_MODE_PRODUCTIVITY, wxS("Productivity ")  , _("Edit files by single-click"));

    AppendSubMenu(dw_om_mode, wxS("Mode"));
}
//  ############################################################################
//
//                          OFLPPanelDataObject
//
//  ############################################################################
OFLPPanelDataObject::OFLPPanelDataObject()
{
    wxDataFormat fmt;
    fmt.SetId(_T("c::b/oflpp-item-drag"));

    SetFormat(fmt);

    a_data_size = 0;
}
//  ============================================================================
bool        OFLPPanelDataObject::p0_serialize   ()
{
    a_data[0]                           =   a_data_panel_index;
    a_data[1]                           =   a_data_item_index;
    *( p0_serialized_editor_pointer() ) =   a_data_editor;

    a_data_size = 2 * sizeof(unsigned char) + sizeof(EditorBase*);

    return true;
}
bool        OFLPPanelDataObject::p0_deserialize ()
{
    a_data_panel_index  =   a_data[0];
    a_data_item_index   =   a_data[1];
    a_data_editor       =   *( p0_serialized_editor_pointer() );
    //  ........................................................................
    //  validity checks
    if ( a_data_panel_index >= OFLPPanelDataObject::s_panel_index_max )
        return false;

    if ( a_data_item_index  >= OFLPPanelDataObject::s_item_index_max  )
        return false;

    if ( ! a_data_editor )
        return false;

    return true;
}
//  ============================================================================
size_t      OFLPPanelDataObject::GetDataSize    ()                     const
{
    GWR_INF("OFLPPanelDataObject::GetDataSize():[%i]", a_data_size);
    return a_data_size;
}

bool        OFLPPanelDataObject::GetDataHere    (void *buf)            const
{
    GWR_INF("%s", _T("OFLPPanelDataObject::GetDataHere():start") );
    if ( ! a_data_size )
        return false;

    memcpy( buf, (void*)a_data, a_data_size );
    return true;
}

int         OFLPPanelDataObject::GetDataPanelIx ()
{
    if ( ! a_data_size )
        return wxNOT_FOUND;

    return static_cast< int >( a_data_panel_index );
}

int         OFLPPanelDataObject::GetDataItemIx  ()
{
    if ( ! a_data_size )
        return wxNOT_FOUND;

    return static_cast< int >( a_data_item_index );
}

EditorBase* OFLPPanelDataObject::GetDataEditor  ()
{
    if ( ! a_data_size )
        return NULL;

    return a_data_editor;
}
//  ============================================================================
bool        OFLPPanelDataObject::SetData        (size_t len, const void *buf)   //  unformatted input serializer
{
    GWR_INF("%s", _T("OFLPPanelDataObject::SetData(1):start") );
    //  ........................................................................
    if ( len > GetDataCapacity() )
        return false;

    memcpy( (void*)a_data, buf, len );

    if ( ! p0_deserialize() )
        return false;

    a_data_size =   len;
    return true;
}

bool        OFLPPanelDataObject::SetData        (int _panel_ix, int _item_ix, EditorBase* _editor)  //  formatted input serializer
{
    GWR_INF("%s", _T("OFLPPanelDataObject::SetData(2):start") );
    //  ........................................................................
    //  input validity checks
    if ( ( _panel_ix < 0 ) || ( _panel_ix >= OFLPPanelDataObject::s_panel_index_max ) )
        return false;

    if ( ( _item_ix < 0  ) || ( _item_ix >= OFLPPanelDataObject::s_item_index_max   ) )
        return false;

    if ( ! _editor )
        return false;
    //  ........................................................................
    //  import data
    a_data_panel_index  =   static_cast< unsigned char >( _panel_ix );
    a_data_item_index   =   static_cast< unsigned char >( _item_ix  );
    a_data_editor       =   _editor;
    //  ........................................................................
    //  now serialize
    return p0_serialize();
}
//  ############################################################################
//
//                          OPENFILESLISTPLUGINPANELITEMDATA
//                          OPENFILESLISTPLUGINPANELITEMINFO
//
//  ############################################################################
OpenFilesListPluginPanelItemData::OpenFilesListPluginPanelItemData(
        OpenFilesListPluginPanel    *   _panel  ,
        EditorBase                  *   _ed     )
            :   a_editor(_ed), a_panel(_panel)
{
}

OpenFilesListPluginPanelItemInfo::OpenFilesListPluginPanelItemInfo(wxTreeEvent& _e)
    :   OpenFilesListPluginPanelItemData    (NULL, NULL )   ,
        a_is_ok                             (false      )
{
    OFLPPanelItemData   *   data;
    //  ........................................................................
    //D GWR_INF("%s", wxS("OFLPPanelItemInfo::()"));
    //  ........................................................................
    a_iid   =   _e.GetItem();
    //D GWR_INF("  iid   [%08x]", a_iid);
    if ( ! a_iid.IsOk() )   goto lab_failure;

    a_tree  = static_cast< wxTreeCtrl* >( _e.GetEventObject() );
    //D GWR_INF("  tree  [%08x]", a_tree);
    if ( ! a_tree )         goto lab_failure;

    //  when DnD ended, OnTreeSelChanged is called on the source ; we land
    //  here with a valid iid but a NULL data, since the item was removed !
    data    = static_cast< OFLPPanelItemData* >( a_tree->GetItemData(a_iid) );
    //D GWR_INF("  data  [%08x]", data);
    if ( ! data )           goto lab_failure;

    a_panel     =   data->GetPanel();
    //D GWR_INF("  panel [%08x]", a_panel);
    if ( ! a_panel )        goto lab_failure;

    a_editor    =   data->GetEditor();
    //D GWR_INF("  editor[%08x]", a_editor);
    if ( ! a_editor )       goto lab_failure;

    #ifdef  GWR_OFLP_SANITY_CHECKS                                              //  _GWR_SANITY_CHECK_
    if ( GetPanel()->editor_index(GetEditor()) == wxNOT_FOUND )
        goto lab_failure;

    if ( GetPanel()->tree() != a_tree )
        goto lab_failure;
    #endif

    a_is_ok     =   true;
    return;
    //  ........................................................................
lab_failure:
    //D GWR_ERR("%s", wxS("  failure"));
    OpenFilesListPluginPanelItemData:p1_raz();
    OpenFilesListPluginPanelItemInfo:p1_raz();
    return;
}
//  ############################################################################
//
//                          OPENFILESLISTPLUGINPANELDROPTARGET
//
//  ############################################################################
OpenFilesListPluginPanelDropTarget::        OpenFilesListPluginPanelDropTarget(wxTreeCtrl* _owner, OFLPPanel* _owner_panel)
{
    a_owner         =   _owner;
    a_owner_panel   =   _owner_panel;
    d_data_object   =   new OFLPPanelDataObject();

    SetDataObject( d_data_object );
}

OpenFilesListPluginPanelDropTarget::       ~OpenFilesListPluginPanelDropTarget()
{
    //delete d_data_object;
}

bool OpenFilesListPluginPanelDropTarget::   OnDrop(wxCoord x, wxCoord y)
{
    GWR_INF("%s", _T("OFLPPanelDropTarget::OnDrop():start") );
    return true;
}

wxDragResult
OpenFilesListPluginPanelDropTarget::        OnData(wxCoord x, wxCoord y, wxDragResult defaultDragResult)
{
    wxDragResult        dres;

    EditorBase      *   deditor;
    OFLPPanel       *   dpanel  =   NULL;
    int                 dpvix   =   0;
    int                 diix    =   0;

    int                 pvix    =   0;
    wxTreeItemId        iid;
    //  ........................................................................
    //  ........................................................................
    //  use predef wxWidgets object copy ; depending on ??? ( answer :
    //  dropSource.DoDragDrop() ) the drag result may differ from what the drop
    //  source has asked for
    dres = wxDropTarget::OnData(x, y, defaultDragResult);

    GWR_INF("OFLPPanelDropTarget::OnData():suggested drag result[%s] returned[%s]"  ,
        OpenFilesListPluginPanel::stringize_drag_result(defaultDragResult).c_str()  ,
        OpenFilesListPluginPanel::stringize_drag_result(dres).c_str()               );

    if ( dres != defaultDragResult )
    {
        d_data_object->ClrData();
        return dres;
    }
    //  ........................................................................
    //  now insert item
    deditor = d_data_object->GetDataEditor();                                   //  get editor from unserialized data
    dpvix   = d_data_object->GetDataPanelIx();                                  //  ...panel visual index
    diix    = d_data_object->GetDataItemIx();                                   //  ...item visual index
    GWR_INF("  data:pvix[%i] iix[%i] ed[%08x]\n", dpvix, diix, deditor);

    #ifdef  GWR_OFLP_SANITY_CHECKS                                              //  _GWR_SANITY_CHECK_
    dpanel  = opanel()->plugin()->GetPanel(deditor);                            //  get panel from editor
    GWR_INF("  panel:from editor[%08x]", dpanel);

    pvix    = opanel()->plugin()->GetPanelVisualIndex(dpanel);
    //iid     = dpanel->item_find(ed);

    if ( dpvix != pvix )
    {
        GWR_ERR("  panel visual index mismatch:[%i]", pvix);
        return wxDragError;
    }
    #endif                                                                      //  _GWR_TODO_ check item index too

    //  insert item
    opanel()->editor_add(deditor);
    opanel()->editor_select(deditor);

    return dres;
}
//  ############################################################################
//
//                          OPENFILESLISTPLUGINPANELHEADER
//
//  ############################################################################
bool OpenFilesListPluginPanelHeader::   p0_title_ctrl_replace       (wxWindow* _vnew)
{
    //  ........................................................................
    GWR_INF("OFLPPanel::p0_title_ctrl_replace():old[%08x] act[%08x] new[%08x]", dw_txt_old, aw_txt, _vnew);
    //  ........................................................................
    if ( dw_txt_old )
        delete dw_txt_old;                                                      //  here old aw_txt is not unused anymore

    if ( ! dw_sizer->Replace( aw_txt, _vnew ) )
    {
        GWR_ERR("%s", _T(" wxSizer::Replace() failed"));
        return false;
    }

    //  mandatory ? yes else the ctrl is still shwon until we delete it
    aw_txt->Hide();                                                             //  calling Hide() makes KILL_FOCUS to be processed !

    dw_sizer->Layout();

    dw_txt_old  =   aw_txt;                                                     //  dont delete old aw_txt here else segfault
    aw_txt      =   _vnew;

    return true;
}

void OpenFilesListPluginPanelHeader::   title_switch_to_dynamic     ()
{
    GWR_INF("%s", wxS("OFLPPanelHeader::title_switch_to_dynamic()"));
    //  ........................................................................
    dw_txt_dyn = new wxTextCtrl( this, wxNewId(), a_title );
    dw_txt_dyn->SetWindowStyle(wxTE_PROCESS_ENTER);                             //  allow wxEVT_COMMAND_TEXT_ENTER generation

    GWR_INF(" new ctrl[%08x]", dw_txt_dyn);

    if ( ! p0_title_ctrl_replace(dw_txt_dyn) )
    {
        delete dw_txt_dyn;
        return;
    }

    dw_txt_dyn->Connect(
        wxEVT_COMMAND_TEXT_ENTER                                        ,
        wxCommandEventHandler(OFLPPanel::evh_title_dynamic_TEXT_ENTER)  ,
        NULL, aw_parent                                                 );      //  callback is on OFLPPanel

    dw_txt_dyn->Connect(                                                        //  loosing the focus is like enter
        wxEVT_KILL_FOCUS                                                ,
        wxFocusEventHandler(OFLPPanel::evh_title_dynamic_KILL_FOCUS)    ,
        NULL, aw_parent                                                 );      //  callback is on OFLPPanel

    dw_txt_dyn->SetFocus();                                                     //  so the user can input text immediately
}

void OpenFilesListPluginPanelHeader::   title_switch_to_static      ()
{
    GWR_INF("%s", wxS("OFLPPanelHeader::title_switch_to_static()"));
    //  ........................................................................
    a_title     = dw_txt_dyn->GetLineText(0);
    dw_txt_sta  = new wxStaticText( this, wxNewId(), a_title );

    GWR_INF(" new ctrl[%08x]", dw_txt_sta);

    if ( ! p0_title_ctrl_replace(dw_txt_sta) )
    {
        delete dw_txt_sta;
        return;
    }

    dw_txt_sta->Connect(
        wxEVT_LEFT_DOWN                                             ,
        wxMouseEventHandler(OFLPPanel::evh_title_static_LEFT_DOWN)  ,
        NULL,aw_parent                                              );          //  callback is on OFLPPanel
}
//  ============================================================================
wxButton*
OpenFilesListPluginPanelHeader::        button(int _ix)
{
    if ( _ix < 0 )
        return NULL;

    if ( _ix >= a_buttons_array.size() )
        return NULL;

    return a_buttons_array[ (size_t)_ix ];
}

void OpenFilesListPluginPanelHeader::   button_prepend  (int _bitmap_id)
{
    wxSize          sz(24,16);
    wxButton    *   bt  =   NULL;
    //  ........................................................................
    //  ........................................................................
    bt  = new wxBitmapButton(this, wxNewId()                                ,
        OpenFilesListPlugin::GetImagesList()->GetBitmap(_bitmap_id)         ,
        wxDefaultPosition, wxDefaultSize, wxBORDER_NONE |  wxBU_EXACTFIT    );

    //  wxEventHandler's ClientData is a easy way to pass extra data
    //  with the event
    bt->SetClientData( (void*)aw_parent);

    bt->SetMaxSize(sz);
    bt->SetMinSize(sz);

    a_buttons_array.insert( a_buttons_array.begin(), bt );
    dw_sizer->Prepend( bt, 0, wxEXPAND, 0 );
}

void OpenFilesListPluginPanelHeader::   button_append   (int _bitmap_id)
{
    wxSize          sz(24,16);
    wxButton    *   bt  =   NULL;
    //  ........................................................................
    //  ........................................................................
    bt  = new wxBitmapButton(this, wxNewId()                                ,
        OpenFilesListPlugin::GetImagesList()->GetBitmap(_bitmap_id)         ,
        wxDefaultPosition, wxDefaultSize, wxBORDER_NONE |  wxBU_EXACTFIT    );

    //  wxEventHandler's ClientData is a easy way to pass extra data
    //  with the event
    bt->SetClientData( (void*)aw_parent);

    bt->SetMaxSize(sz);
    bt->SetMinSize(sz);

    a_buttons_array.push_back( bt );
    dw_sizer->Add( bt, 0, wxEXPAND, 0 );
}

void OpenFilesListPluginPanelHeader::   button_show     (int _ix, bool _b)
{
    wxButton    *   bt  =   button(_ix);

    if ( ! bt )
        return;

    bt->Show(_b);
}
//  ============================================================================
OpenFilesListPluginPanelHeader::        OpenFilesListPluginPanelHeader()        {}
OpenFilesListPluginPanelHeader::       ~OpenFilesListPluginPanelHeader()        {}
OpenFilesListPluginPanelHeader::        OpenFilesListPluginPanelHeader(
        OFLPPanel   *   _parent ,
        wxString        _title  )
            :   wxPanel     (_parent, wxNewId())    ,
                aw_parent   (_parent)
{
    //  ........................................................................
    a_title         =   _title;

    dw_sizer        =   new wxBoxSizer(wxHORIZONTAL);

    dw_txt_sta      =   new wxStaticText( this, wxNewId(), _title );            //  at fist display, static text displayed
    dw_txt_dyn      =   NULL;

    aw_txt          =   dw_txt_sta;
    dw_txt_old      =   NULL;                                                   //  no txt has been replaced yet

    dw_sizer->Add( dw_txt_sta, 1, wxEXPAND, 0);

    this->SetSizer(dw_sizer);

    if ( ! aw_parent->is_bulk() )
        dw_txt_sta->Connect(
            wxEVT_LEFT_DOWN                                             ,
            wxMouseEventHandler(OFLPPanel::evh_title_static_LEFT_DOWN)  ,
            NULL,aw_parent                                              );
}
//  ############################################################################
//
//                          OPENFILESLISTPLUGINPANEL
//
//  ############################################################################
wxString
OpenFilesListPluginPanel::      stringize_drag_result(wxDragResult _dres)
{
    wxString    s = wxString::FromUTF8("invalid");

    switch ( _dres )
    {
        //  Error prevented the D&D operation from completing.
        case    wxDragError :	s = wxString::FromUTF8("wxDragError"); break;

        //  Drag target didn't accept the data.
        case    wxDragNone 	:   s = wxString::FromUTF8("wxDragNone"); break;

        //  The data was successfully copied.
        case    wxDragCopy  :   s = wxString::FromUTF8("wxDragCopy"); break;

        //  The data was successfully moved (MSW only).
        case    wxDragMove  : 	s = wxString::FromUTF8("wxDragMove"); break;

        //  Operation is a drag-link.
        case    wxDragLink  : 	s = wxString::FromUTF8("wxDragLink"); break;

        //  The operation was cancelled by user (not an error).
        case    wxDragCancel:   s = wxString::FromUTF8("wxDragCancel"); break;
    }

    return s;
}

void OpenFilesListPluginPanel:: OnDragInit  (wxTreeEvent& _e)
{
    wxDragResult            dres;
    OFLPPanelDataObject     dobj;
    wxTreeItemId            iid     =   _e.GetItem();
    wxTreeItemData      *   idata   =   d_tree->GetItemData(iid);
    OFLPPanelItemData   *   pidata  =   NULL;
    EditorBase          *   editor  =   NULL;
    //  ........................................................................
    GWR_INF("%s", _T("OFLPPanel::OnDragInit():start") );
    GWR_INF("OFLPPanel::OnDragInit():object format count [%i]", dobj.GetFormatCount());
    //  ........................................................................
    //  You can init a drag anywhere inside the wxTreeCtrl, even where there is
    //  nothing ! In this case, iid IsOk() ( why ??? ) , but idata is NULL
    if ( ! iid.IsOk() )
        return;
    if ( ! idata )
        return;
    pidata  =   static_cast< OFLPPanelItemData* >( idata );
    editor  =   pidata->GetEditor();
    //  ........................................................................
    //  prepare drag source
    dobj.SetData( plugin()->GetPanelVisualIndex(this) , 0xab, editor );         //  _GWR_TODO_ iid
    wxDropSource        dropSource(dobj, d_tree);
    //  ........................................................................
    // do DnD
    dres = dropSource.DoDragDrop( wxDrag_DefaultMove );
    if ( dres != wxDragMove )
    {
        GWR_ERR("OFLPPanel::OnDragInit():res[%s], expected wxDragMove", OpenFilesListPluginPanel::stringize_drag_result(dres).c_str() );
        return;
    }
    //  ........................................................................
    //  delete our item ; this will cause a SELECTION_CHANGE event with a       //  _GWR_TECH_
    //  NULL data
    editor_del( static_cast< OFLPPanelItemData* >( idata )->GetEditor() );
}

void OpenFilesListPluginPanel:: OnDragEnd   (wxTreeEvent& _e)
{
    GWR_INF("%s", _T("OFLPPanel::OnDragEnd()") );
}
//  ============================================================================
void OpenFilesListPluginPanel:: p0_allow_kill_focus_event           (bool _b)
{
    //  When user enters a new panel name, TEXT_ENTER _AND_ KILL_FOCUS are
    //  called ( in this order, but whatever, the two are called ). Thats why
    //  we need some kind of flag, avoiding to delete the same object twice
    //  in p0_title_ctrl_replace()
    a_allow_kill_focus_event = _b;
}

void OpenFilesListPluginPanel:: evh_title_static_LEFT_DOWN          (wxMouseEvent& _e)
{
    //  let OFLPPanelHeader do his job
    dw_header->title_switch_to_dynamic();

    //  the header's events is connected to this ( OpenFilesListPluginPanel )
    //  only for updating this's layout, because wxStaticText and WxwxTextCtrl
    //  dont have same vertical size
    dw_sizer->Layout();
}

void OpenFilesListPluginPanel:: evh_title_dynamic_TEXT_ENTER        (wxCommandEvent& _e)
{
    GWR_INF("%s", _T("OFLPPanel:: evh_title_dynamic_TEXT_ENTER()"));

    p0_allow_kill_focus_event(false);

    dw_header   ->title_switch_to_static();
    dw_sizer    ->Layout();

    p0_allow_kill_focus_event(true);

    _e.Skip();
}

void OpenFilesListPluginPanel:: evh_title_dynamic_KILL_FOCUS        (wxFocusEvent   & _e)
{
    GWR_INF("%s", _T("OFLPPanel:: evh_title_dynamic_KILL_FOCUS()"));

    if ( ! a_allow_kill_focus_event )
    {
        GWR_INF("%s", _T("  (aborted)"));
        return;
    }

    dw_header   ->title_switch_to_static();
    dw_sizer    ->Layout();

    _e.Skip();
}
//  ============================================================================
void OpenFilesListPluginPanel:: p0_create_tree()
{
    d_tree          =   new wxTreeCtrl( this, wxNewId(), wxDefaultPosition, wxSize(150, 100),
        wxTR_HAS_BUTTONS | wxNO_BORDER | wxTR_HIDE_ROOT );
    d_tree->AddRoot( wxString::FromUTF8("ROOT") );

    d_tree->SetImageList(OpenFilesListPlugin::GetImagesList());

    d_tree->Connect(wxEVT_COMMAND_TREE_BEGIN_DRAG,
        wxTreeEventHandler(OpenFilesListPluginPanel::OnDragInit), NULL, this);

    d_tree->Connect(wxEVT_COMMAND_TREE_END_DRAG,
        wxTreeEventHandler(OpenFilesListPluginPanel::OnDragEnd), NULL, this);

    d_tree->Connect(wxEVT_COMMAND_TREE_ITEM_ACTIVATED,
        wxTreeEventHandler(OpenFilesListPlugin::OnTreeItemActivated)    , NULL, a_ofl_plugin);
    d_tree->Connect(wxEVT_COMMAND_TREE_ITEM_RIGHT_CLICK,
        wxTreeEventHandler(OpenFilesListPlugin::OnTreeItemRightClick)   , NULL, a_ofl_plugin);
    d_tree->Connect(wxEVT_COMMAND_TREE_SEL_CHANGED,
        wxTreeEventHandler(OpenFilesListPlugin::OnTreeSelChanged)       , NULL, a_ofl_plugin);

    d_drop_target   =   new OpenFilesListPluginPanelDropTarget(d_tree, this);
    d_tree->SetDropTarget(d_drop_target);

}
//  ============================================================================
OpenFilesListPluginPanel::OpenFilesListPluginPanel(
        OpenFilesListPlugin *   _ofl_plugin ,
        wxWindow            *   _parent ,
        wxString                _title  ,
        bool                    _bulk   )
        :   wxPanel                     ( _parent, wxNewId()    )   ,
            a_ofl_plugin                ( _ofl_plugin   )   ,
            a_bulk                      ( _bulk         )   ,
            a_allow_kill_focus_event    ( true          )
{
    GWR_INF("OpenFilesListPluginPanel::OpenFilesListPluginPanel():[%08x] plugin[%08x]", this, _ofl_plugin);

    dw_header   =   new OpenFilesListPluginPanelHeader( this, _title );

    dw_header->button_prepend   ( 9 );
    dw_header->button_append    ( 7 );
    dw_header->button_append    ( 8 );
    dw_header->button_append    ( 5 );

    dw_header->button(0)->Connect(                                              //  (mini | maxi) mize
        wxEVT_COMMAND_BUTTON_CLICKED                                                    ,
        wxCommandEventHandler(OpenFilesListPlugin::evh_panel_header_button_mm_CLICKED)  ,
        NULL, _ofl_plugin                                                               );

    dw_header->button(1)->Connect(                                              //  up
        wxEVT_COMMAND_BUTTON_CLICKED                                                    ,
        wxCommandEventHandler(OpenFilesListPlugin::evh_panel_header_button_up_CLICKED)  ,
        NULL, _ofl_plugin                                                               );

    dw_header->button(2)->Connect(                                              //  down
        wxEVT_COMMAND_BUTTON_CLICKED                                                        ,
        wxCommandEventHandler(OpenFilesListPlugin::evh_panel_header_button_down_CLICKED)    ,
        NULL, _ofl_plugin                                                                   );

    dw_header->button(3)->Connect(                                              //  del
        wxEVT_COMMAND_BUTTON_CLICKED                                                    ,
        wxCommandEventHandler(OpenFilesListPlugin::evh_panel_header_button_del_CLICKED) ,
        NULL, _ofl_plugin                                                               );

    p0_create_tree();

    dw_sizer    =   new wxBoxSizer(wxVERTICAL);

    dw_sizer->Add( dw_header , 0, wxEXPAND, 0);
    dw_sizer->Add( d_tree    , 1, wxEXPAND, 0);

    this->SetSizer(dw_sizer);
}

OpenFilesListPluginPanel::~OpenFilesListPluginPanel()
{
}
//  ============================================================================
void            OpenFilesListPluginPanel::  reset()
{
}
//  ============================================================================
wxTreeItemId    OpenFilesListPluginPanel::  item_find           (EditorBase* _editor)
{
    wxTreeItemIdValue   cookie  =   0;
    wxTreeItemId        iid     = d_tree->GetFirstChild( d_tree->GetRootItem(), cookie );
    //  ........................................................................
    //D GWR_INF("%s", _T("OpenFilesListPluginPanel::item_find()"));
    //  ........................................................................
    // loop all tree items
    while ( iid )
    {
        OFLPPanelItemData       *   data    = static_cast<OFLPPanelItemData*>(d_tree->GetItemData(iid));
        EditorBase              *   e       = data->GetEditor();
        if ( _editor == e )
        {
            //D GWR_INF("%s", _T("OpenFilesListPluginPanel::item_find():found"));
            return iid;
        }
        iid = d_tree->GetNextChild(d_tree->GetRootItem(), cookie);
    }
    //D GWR_INF("%s", _T("OpenFilesListPluginPanel::item_find():not found"));
    iid.Unset();
    return iid;
}

wxTreeItemId    OpenFilesListPluginPanel::  item_append         (EditorBase* _editor)
{
    wxTreeItemId        iid;
    int                 mod = GetOpenFilesListIcon(_editor);
    //  ........................................................................
    //  ........................................................................
    iid = d_tree->AppendItem(
        d_tree->GetRootItem()   ,
        _editor->GetShortName() ,
        mod, mod                ,
        new OFLPPanelItemData(this, _editor)   );

    if ( ! iid.IsOk() )
    {
        GWR_ERR("%s", _T("OpenFilesListPluginPanel::item_append():failed"));
        iid.Unset();
        return iid;
    }

    GWR_INF("OpenFilesListPluginPanel::item_append():success img[%i]", mod);

    d_tree->SortChildren(d_tree->GetRootItem());

    return iid;
}

bool            OpenFilesListPluginPanel::  item_del            (EditorBase* _editor)
{
    wxTreeItemId    iid = item_find(_editor);
    //  ........................................................................
    if ( ! iid.IsOk() )
    {
        return false;
    }

    d_tree->Delete(iid);
    return true;
}

bool            OpenFilesListPluginPanel::  item_select         (EditorBase* _editor)
{
    wxTreeItemId    iid = item_find(_editor);
    //  ........................................................................
    if ( ! iid.IsOk() )
    {
        return false;
    }

    if ( d_tree->GetSelection() == iid )
        return true;

    d_tree->SelectItem(iid);
    return true;
}

void            OpenFilesListPluginPanel::  items_deselect      ()
{
    d_tree->UnselectAll();
}

void            OpenFilesListPluginPanel::  items_del           ()
{
    d_tree->DeleteChildren(d_tree->GetRootItem());
}
//  ============================================================================
int             OpenFilesListPluginPanel::  editor_index    (EditorBase* _editor)
{
    return a_editors_array.Index(_editor);
}

wxTreeItemId    OpenFilesListPluginPanel::  editor_add      (EditorBase* _editor)
{
    wxTreeItemId    iid;
    int             mod =   GetOpenFilesListIcon(_editor);
    //  ........................................................................
    GWR_INF("%s", _T("OFLPPanel::editor_add()"));
    //  ........................................................................
    iid = item_append(_editor);

    if ( ! iid.IsOk() )
    {
        return iid;
    }

    a_editors_array.Add(_editor);

    //d_tree->SetItemImage(iid, mod, wxTreeItemIcon_Normal);
    //d_tree->SetItemImage(iid, mod, wxTreeItemIcon_Selected);

    return iid;
}

void            OpenFilesListPluginPanel::  editor_del      (EditorBase* _editor)
{
    //  ........................................................................
    GWR_INF("%s", _T("OFLPPanel::editor_del()"));
    //  ........................................................................
    item_del(_editor);

    a_editors_array.Remove(_editor);
}

void            OpenFilesListPluginPanel::  editor_select   (EditorBase* _editor)
{
    //  ........................................................................
    GWR_INF("%s", _T("OFLPPanel::editor_select()"));
    //  ........................................................................
    item_select(_editor);
}

void            OpenFilesListPluginPanel::  editor_sync     (EditorBase* _editor)
{
    wxTreeItemId    iid;
    int             mod =   GetOpenFilesListIcon(_editor);
    //  ........................................................................
    GWR_INF("%s", _T("OFLPPanel::editor_refresh()") );
    //  ........................................................................
    iid = item_find(_editor);

    if ( ! iid.IsOk() )
    {
        return;
    }

    if ( d_tree->GetItemImage(iid, wxTreeItemIcon_Normal)  != mod )
    {
        d_tree->SetItemImage( iid, mod , wxTreeItemIcon_Normal );
        d_tree->SetItemImage( iid, mod , wxTreeItemIcon_Selected );
    }
    if ( d_tree->GetItemText(iid)   != _editor->GetShortName() )
    {
        d_tree->SetItemText( iid, _editor->GetShortName() );
        d_tree->SortChildren( d_tree->GetRootItem() );
    }
}

void            OpenFilesListPluginPanel::  editors_del     ()
{
    //  ........................................................................
    GWR_INF("%s", _T("OFLPPanel::editors_del()"));
    //  ........................................................................
    a_editors_array.Clear();
    items_del();
}

void            OpenFilesListPluginPanel::  editors_deselect()
{
    //  ........................................................................
    GWR_INF("%s", _T("OFLPPanel::editors_deselect()"));
    //  ........................................................................
    items_deselect();
}
//  ============================================================================
bool            OpenFilesListPluginPanel::  is_minimized    ()
{
    return ! d_tree->IsShown();
}

void            OpenFilesListPluginPanel::  minimize        ()
{
    d_tree->Show(false);

    dw_header->button_show( 1, false);
    dw_header->button_show( 2, false);
}

void            OpenFilesListPluginPanel::  maximize        ()
{
    d_tree->Show(true);

    dw_header->button_show( 1, true);
    dw_header->button_show( 2, true);
}
//  ############################################################################
//
//                          OPENFILESLISTPLUGINPANELBULK
//
//  ############################################################################
OpenFilesListPluginPanelBulk::OpenFilesListPluginPanelBulk(
        OpenFilesListPlugin *   _ofl_plugin ,
        wxWindow            *   _parent ,
        wxString                _title  )
        :   OpenFilesListPluginPanel( _ofl_plugin, _parent, _title, true)   ,
            dw_menu_main                            (NULL)  ,
                dw_menu_selection_mode              (NULL)  ,
                    dw_item_selection_standard      (NULL)  ,
                    dw_item_selection_productivity  (NULL)
{
    dw_header   =   new OpenFilesListPluginPanelHeader( this, _title );

    dw_header->button_prepend   ( 6 );
    dw_header->button_append    ( 4 );

    dw_header->button(0)->Connect(                                              //  options
        wxEVT_COMMAND_BUTTON_CLICKED                                                    ,
        wxCommandEventHandler(OpenFilesListPlugin::evh_panel_header_button_opt_CLICKED) ,
        NULL, _ofl_plugin                                                               );

    dw_header->button(1)->Connect(                                              //  add panel
        wxEVT_COMMAND_BUTTON_CLICKED                                                    ,
        wxCommandEventHandler(OpenFilesListPlugin::evh_panel_header_button_add_CLICKED) ,
        NULL, _ofl_plugin                                                               );

    p0_create_tree();

    dw_sizer    =   new wxBoxSizer(wxVERTICAL);

    dw_sizer->Add( dw_header , 0, wxEXPAND, 0);
    dw_sizer->Add( d_tree    , 1, wxEXPAND, 0);

    this->SetSizer(dw_sizer);
}

OpenFilesListPluginPanelBulk::~OpenFilesListPluginPanelBulk()
{
}
