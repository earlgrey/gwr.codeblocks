/*
 * This file is part of the Code::Blocks IDE and licensed under the GNU General Public License, version 3
 * http://www.gnu.org/licenses/gpl-3.0.html
 */

#ifndef OPENFILESLISTPLUGIN_H
#define OPENFILESLISTPLUGIN_H

#include <cbplugin.h>

#include <wx/dynarray.h>
#include <wx/menu.h>

#include "openfileslistpluginpanel.h"

class wxTreeCtrl;
class wxTreeEvent;
class wxMenuBar;
class wxImageList;
class EditorBase;

class   Location;

//WX_DEFINE_ARRAY(EditorBase              *, EditorArray);

class OpenFilesListPlugin : public cbPlugin
{
    friend class OpenFilesListPluginPanel;

    public:
        OpenFilesListPlugin();
        virtual ~OpenFilesListPlugin();

        virtual int GetConfigurationGroup() const { return cgEditor; }

        virtual void BuildMenu(wxMenuBar* menuBar);

        virtual void OnAttach();
        virtual void OnRelease(bool appShutDown);
    protected:
        //int GetOpenFilesListIcon(EditorBase* ed);
        void RebuildOpenFilesTree();
        void RefreshOpenFilesTree(EditorBase* ed, bool remove = false);

        void OnTreeItemActivated(wxTreeEvent& event);
        void OnTreeItemRightClick(wxTreeEvent& event);
        void OnTreeSelChanged(wxTreeEvent &event);
        void OnViewOpenFilesTree(wxCommandEvent& event);
        void OnUpdateUI(wxUpdateUIEvent& event);

        void OnEditorActivated(CodeBlocksEvent& event);
        void OnEditorClosed(CodeBlocksEvent& event);
        void OnEditorDeactivated(CodeBlocksEvent& event);
        void OnEditorModified(CodeBlocksEvent& event);
        void OnEditorOpened(CodeBlocksEvent& event);
        void OnEditorSaved(CodeBlocksEvent& event);

        void OnProjectOpened(CodeBlocksEvent& event);

                wxMenu          *   m_ViewMenu;

    private:
        EditorArray m_EditorArray;
    //  ========================================================================
  private:
    static          wxImageList     s_img_list;

  public:
    static  const   int             OPT_MODE_STANDARD       =   0x0001;
    static  const   int             OPT_MODE_PRODUCTIVITY   =   0x0002;

  private:
    wxPanel                         *   d_MainPanel;
    wxBoxSizer                      *   d_MainSizer;
    OpenFilesListPluginPanel        *   d_BulkPanel;

  private:
    int                                 a_option_mode;

    OpenFilesListPluginOptionsMenu  *   dw_options_menu;

  private:
    PanelArray  a_panels_array;
    //  --------------------------------------------------------------------
  public:
    static wxImageList  *   GetImagesList() { return &s_img_list;   }

    void                    ResetPanels();

    OFLPPanel           *   AddPanel(wxString _title, bool _bulk = false);
    void                    MovePanelUp(OFLPPanel*);
    void                    MovePanelDn(OFLPPanel*);
    void                    DelPanel(OFLPPanel*);
    void                    DeselectItemsOfAllPanelsButOne(OFLPPanel*);

    OFLPPanel           *   GetPanel            (EditorBase*);
    int                     GetPanelVisualIndex (OFLPPanel*);

    OFLPOptionsMenu     *   GetOptionsMenu()    { return dw_options_menu;   }

    bool                    mode_standard()     { return ( a_option_mode == OPT_MODE_STANDARD       );  }
    bool                    mode_productivity() { return ( a_option_mode == OPT_MODE_PRODUCTIVITY   );  }
    //  --------------------------------------------------------------------
    void                    evh_panel_header_button_add_CLICKED     (wxCommandEvent &);
    void                    evh_panel_header_button_del_CLICKED     (wxCommandEvent &);
    void                    evh_panel_header_button_mm_CLICKED      (wxCommandEvent &);
    void                    evh_panel_header_button_up_CLICKED      (wxCommandEvent &);
    void                    evh_panel_header_button_down_CLICKED    (wxCommandEvent &);
    void                    evh_panel_header_button_opt_CLICKED     (wxCommandEvent &);

    void                    evh_menu_option_checked                 (wxCommandEvent&);
    //  ========================================================================
        DECLARE_EVENT_TABLE();
};



#endif                                                                          // OPENFILESLISTPLUGIN_H
