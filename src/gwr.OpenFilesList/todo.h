 (001)  2015.08.21  Obtain keyboard focus in text control in OFLPPanelHeader
                    when changing the title

 (002)  2015.08.21  Terminate standard / productivity mode

+(003)  2015.08.21  Rescan all old methods for compatibility

+(004)  2015.08.21  When editor change from ofl, focus stay on ofl, is not given
                    to editor

 (005)  2015.08.21  When editing OFLPPanel title, the wxTextCtrl remains if
                    user click anywhere else ; should switch back to wxStaticText

+(006)  2015.08.21  Save layout & options

+(007)  2015.08.21  After DnD selection of item is lost

+(008)  2015.08.21  Verify icons

+(009)  2015.08.21  Double loop : mouse -> activate editor -> select editor -> OnTreeItemSelected -> refresh -> select editor

 (010)  2015.08.21  Possibility to hide the wxTreeCtrl

 (011)  2015.08.22  Double call to OFLPPanelHeader::switch_to_static()
                    because of TEXT_ENTER _AND_ KILL_FOCUS events

 (012)  2015.08.22  Hide up & down buttons when panel is minimized
//  ============================================================================
(001)
2015.08.21  Done: call to SetFocus() )
//  ============================================================================
(002)
2015.08.21  Done
//  ============================================================================
(003)
2015.08.21  done : cbEVT_EDITOR_ACTIVATED
              RefreshOpenFilesTree() : select item if editor activated from tabs
2015.08.21  done : cbEVT_EDITOR_MODIFIED
            done : cbEVT_EDITOR_SAVED
              RefreshOpenFilesTree() : name + icon + tree re-sorting
//  ============================================================================
(004)
//  ============================================================================
(005)
2015.08.21  Done: by handling wxTextCtrl wxEVT_KILL_FOCUS event
//  ============================================================================
(006)
//  ============================================================================
(007)
//  ============================================================================
(008)
//  ============================================================================
(009)
//  ============================================================================
(010)
2015.08.22  done
//  ============================================================================
(011)
2015.08.23  done : flag OFLPPanel::a_allow_kill_focus_event
//  ============================================================================
(012)
2015.08.22  done
