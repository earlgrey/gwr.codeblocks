#!/bin/bash

################################################################################
#                                                                              #
#       THIS SCRIPT _MUST_ BE RUN FROM THE DIRECTORY IT RESIDES IN             #
#                                                                              #
################################################################################

#   ############################################################################
#   build.bash
#
#   Main build script for gwr.codeblocks
#
#   Options :
#   -r : targets to build ( -r = svn10373 )
#   -x : include Scintilla-LexerExt
#   -f : include OpenFileList modification
#   ############################################################################
#   ############################################################################
#   NOTES
#   ############################################################################
#   ############################################################################
#   INCLUDES
#   ############################################################################
source  /usr/local/bin/gwr.cb-build-helper.include.bash
#   ############################################################################
#   VARIABLES
#   ############################################################################
GError=""
GSpaces=""
GSpacesStep="    "

SlcCbVersion=""

SlcBuildOfl="no"
SlcBuildLex="no"
#   ############################################################################
#   FUNCTIONS
#   ############################################################################

#   ############################################################################
#   MAIN
#   ############################################################################
reset

cbh_func__log   "############################################################################"
cbh_func__log   "script : [build.bash]"
cbh_func__log   "############################################################################"

#   verify we are run from the directory we reside in
cbh_func__log   "> verifying pwd ..."
cd  $( pwd )
if [[ ! -f "root-directory.txt" ]] ; then
    cbh_func__log   "  some files were not found. Ensure running this script from the directory it resides in."
    exit 1
fi

#   build directories names
SlcRootDir=$( pwd )
SleRootDir="/home/gwr/Src/C-C++/Scintilla-LexerExt"
SleLEDir="${SleRootDir}/src/01-lexerext"
SlePLDir="${SleRootDir}/src/03-languages"

#   get programs versions, programmation languages list
SlePLs=$( cat "${SleRootDir}/languages.txt" )
SlePLCard=0 ; for l in ${SlePLs} ; do
    SlePLCard=$(( SlePLCard + 1 ))
done

#   get options
while getopts "v:fx" opt; do
    case ${opt} in

        v)
        SlcCbVersion="${OPTARG}"
        ;;

        f)
        SlcBuildOfl="yes"
        ;;

        x)
        SlcBuildLex="yes"
        ;;

        \?)
        cbh_func__log   "> Invalid option: -${OPTARG}"
        ;;

        :)
        echo "Option -$OPTARG requires an argument."
        ;;

    esac
done

# if no c::b version selected, exit
if [[ -z "${SlcCbVersion}" ]] ; then
    cbh_func__log   "no codeblocks version selected ( -v )"
    exit 1
fi

#   get already the lexer id ( SlcCbLexerId )
source  "${SlcRootDir}/src/${SlcCbVersion}/lexer-id.include.bash"

cbh_func__log   "> directory for SLC root            :[${SlcRootDir}]"
cbh_func__log   "> codeblocks version                :[${SlcCbVersion}]"
cbh_func__log   "> include Scintilla-LexerExt        :[${SlcBuildLex}]"
if [[ "${SlcBuildLex}" = "yes" ]] ; then
cbh_func__log   "  directory for SLE root            :[${SleRootDir}]"
cbh_func__log   "  directory for SLE lexerext        :[${SleLEDir}]"
cbh_func__log   "  directory for SLE languages       :[${SlePLDir}]"
cbh_func__log   "                SLE languages       :[${SlePLCard}]"
fi
cbh_func__log   "> include OpenFilesList modification:[${SlcBuildOfl}]"
if [[ "${SlcBuildOfl}" = "yes" ]] ; then
cbh_func__log   "                                    :* TODO *"
fi


#   export some vars for sub-scripts
export  GError
export  GSpaces
export  GSpacesStep

export  SlcRootDir
export  SlcCbVersion
export  SlcCbLexerId
export  SlcBuildLex
export  SlcBuildOfl
export  SleRootDir
export  SleLEDir
export  SlePLDir
export  SlePLs
export  SlePLCard


#   go !
cbh_func__cd_and_build "" "${SlcRootDir}/src"
cbh_func__check_errors $?

exit 0
